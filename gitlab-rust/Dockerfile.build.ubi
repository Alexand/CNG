ARG UBI_IMAGE=registry.access.redhat.com/ubi9/ubi-minimal:9.5

FROM ${UBI_IMAGE}

ARG RUST_VERSION=1.73.0
ARG RUST_PLATFORM=x86_64-unknown-linux-gnu
ARG BUILD_DIR=/tmp/build
ARG DNF_OPTS

ENV RUST_URL="https://static.rust-lang.org/dist/rust-$RUST_VERSION-$RUST_PLATFORM.tar.gz"

# Install Rust
RUN mkdir ${BUILD_DIR} \
    && microdnf ${DNF_OPTS} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 tar gzip \
    && cd ${BUILD_DIR} \
    && curl --retry 6 -sfo rust.tar.gz ${RUST_URL} \
    && curl --retry 6 -sfo rust.tar.gz.asc ${RUST_URL}.asc \
    && curl --retry 6 -sfo rust_signing_key.pub  https://static.rust-lang.org/rust-key.gpg.ascii \
    && gpg2 --import rust_signing_key.pub \
    && gpg2 --auto-key-retrieve --verify rust.tar.gz.asc rust.tar.gz \
    && tar -xzf rust.tar.gz \
    && rust-$RUST_VERSION-$RUST_PLATFORM/install.sh --components=rustc,cargo,rust-std-$RUST_PLATFORM --destdir=/assets \
    && rm rust.tar.gz \
    && rm -rf ${BUILD_DIR} \
    && microdnf clean all -y
