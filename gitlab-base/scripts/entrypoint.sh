#!/bin/bash

set -e

# If in RHEL / UBI images, set ENV.
# NOTICE: ubi-micro does not have grep
if [ -f /etc/redhat-release ]; then
  export HOME=/home/${GITLAB_USER}
  export USER=${GITLAB_USER}
  export USERNAME=${GITLAB_USER}
fi

# If detected "system-fips" file, enforce FIPS mode defaults.
if [ -f /etc/system-fips ]; then
  export FIPS_MODE=${FIPS_MODE-1}
  export OPENSSL_FORCE_FIPS_MODE=${OPENSSL_FORCE_FIPS_MODE-1}
  # https://www.gnutls.org/manual/html_node/FIPS140_002d2-mode.html
  export GNUTLS_FORCE_FIPS_MODE=${GNUTLS_FORCE_FIPS_MODE-3} # 1: enfoce, 3: lax, 4: log
  # https://github.com/golang-fips/go?tab=readme-ov-file#openssl-support
  export GOLANG_FIPS=${GOLANG_FIPS-1}
fi

/scripts/set-config "${CONFIG_TEMPLATE_DIRECTORY}" "${CONFIG_DIRECTORY:=$CONFIG_TEMPLATE_DIRECTORY}"

if [ "${USE_TINI-1}" -eq 1 ]; then
  INIT_CMD="/usr/bin/tini --"
fi

exec ${INIT_CMD} /scripts/exec-env "$@"
